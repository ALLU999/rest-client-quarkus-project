package com.allu.quarkus.crypto.service;

import com.allu.quarkus.crypto.data.Currency;
import com.allu.quarkus.crypto.data.MutlipartBody;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@RegisterRestClient(configKey = "config.api.crypto")
//@Path("ticker")
public interface CurrencyService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Currency> getCurrency(@QueryParam("id") String id);

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String sendFile(@MultipartForm MutlipartBody body);


}
