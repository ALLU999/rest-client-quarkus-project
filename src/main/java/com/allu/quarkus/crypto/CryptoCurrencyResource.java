package com.allu.quarkus.crypto;

import com.allu.quarkus.crypto.data.MutlipartBody;
import com.allu.quarkus.crypto.service.CurrencyService;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;

@Path("/crypto")
public class CryptoCurrencyResource {

    @Inject
    @RestClient
     CurrencyService service;

/*    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }
*/

/*    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Object getCurrencies(@QueryParam("id") String id)  {
        System.out.println("inside resource");
                return service.getCurrency(id);
    }
*/
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String echoFile(String body){
        System.out.println("dsd");
        return body;
    }


    @Path("/test")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String callEcho(){
        final MutlipartBody mutlipartBody = new MutlipartBody();
        mutlipartBody.setFile(new ByteArrayInputStream("Hello World".getBytes()));
        mutlipartBody.setName("hello.txt");
        return service.sendFile(mutlipartBody);
    }

}