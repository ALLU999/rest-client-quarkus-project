package com.allu.quarkus.crypto;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeCryptoCurrencyResourceIT extends CryptoCurrencyResourceTest {

    // Execute the same tests but in native mode.
}